<?php

namespace App\GraphQL\Queries;

use App\Models\User;
use GraphQL\Error\Error;

class Profile
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        /** @var User*/
        $user = auth()->user();

        if(!$user) {
            throw new Error("Проблема с авторизацией");
        }

        return $user;
    }
}
