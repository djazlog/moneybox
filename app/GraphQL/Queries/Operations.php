<?php

namespace App\GraphQL\Queries;

use GraphQL\Error\Error;

class Operations
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {

        $user = auth()->user();

        if (!$user) {
            throw new Error("Не авторизирован");
        }

        return $user
            ->operations()
            ->paginate();
    }
}
