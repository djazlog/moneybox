<?php

namespace App\GraphQL\Queries;

use GraphQL\Error\Error;

class Document
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {

        $user = auth()->user();

        if (!$user) {
            throw new Error("Не авторизирован");
        }

        return $user->documents;
    }
}
