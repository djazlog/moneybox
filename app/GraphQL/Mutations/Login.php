<?php

namespace App\GraphQL\Mutations;

use App\Models\User;
use GraphQL\Error\Error;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class Login
{

    /**
     * @param null $_
     * @param array<string, mixed> $args
     */
    public function __invoke($_, array $args)
    {

        $guard = Auth::guard(config('sanctum.guard', 'web'));

        $this->tryRegister($args);

        if (!$guard->attempt(['phone' => $args['phone'], 'password' => $args['password']])) {
            throw new Error('Invalid credentials.');
        }

        /**
         * Since we successfully logged in, this can no longer be `null`.
         *
         * @var \App\Models\User $user
         */
        $user = $guard->user();

        return $user->createToken('token-auth')->plainTextToken;
    }

    /**
     * TODO: Заменить на отправку смс кода
     * @param $args
     */
    private function tryRegister($args)
    {
        $functionCreate = function () use ($args) {
            $user = User::query()
                ->where("phone", $args['phone'])
                ->first();

            if (!$user) {
                $data = [
                    'phone' => $args['phone'],
                    'password' => Hash::make($args['password']),
                ];
                User::create($data);
            }
        };

        DB::transaction($functionCreate);
    }
}
