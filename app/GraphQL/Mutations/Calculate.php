<?php

namespace App\GraphQL\Mutations;

use Carbon\Carbon;

class Calculate
{
    /**
     * @param null $_
     * @param array<string, mixed> $args
     */
    public function __invoke($_, array $args)
    {

        $x = $args['start'];  // нач. вклад
        $p = $args['procent'];   // год. процент
        $t = $args['years'];     // кол-во лет
        $add = $args['add'];     // Ежемесячная прибавка

        if ($p > 0) {
            $p = $p / 100;
        } else {
            $p = 0;
        }

        $data = [];
        for ($i = 0; $i < $t; $i++) {
            if($i == 0){
                $x += $add * (12 - Carbon::now()->month + 1);
            }else {
                $x += $add * 12;
            }
            $data[] = round($x * pow(1 + $p, $i), 2) . " - ". $p;
        }
        return $data;

    }
}
