<?php

namespace App\GraphQL\Mutations;

use GraphQL\Error\Error;

class SaveSettings
{
    /**
     * @param null $_
     * @param array<string, mixed> $args
     */
    public function __invoke($_, array $args)
    {
        $user = auth()->user();

        if (!$user) {
            throw new Error("Не авторизирован");
        }

        $data = $args['input'];

        $user
            ->setting()
            ->update($data);

        return $user->setting;
    }
}
