<?php

use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/chart', function () {
    $chart = new ImageCharts();
    $pie = $chart
        ->cht('bvs')
        ->chbr('10')
        ->chco('CFECF7,27c9c2')
        //->chl('Года|World')
        ->chd('t:40,40,60,80,30,10,28,10,32,43,23,55,43,22,99')
        ->chs('600x400');

    //echo $pie->toURL(); // https://image-charts.com/chart?chd=a%3A2.5%2C5%2C8.3&chs=600x300&cht=p
    //$pie->toFile('/path/to/chart.png'); //

   // echo $pie->toDataURI(); // data:image/png;base64,iVBORw0KGgo...
    header("Content-type: image/png");
    echo $pie->toBinary();
    //return view('welcome');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
